# Micromouse  
## Andrew ##
## Brian ##
## David ##
## Nick ##


#### Version 2: Outline #####

### Overview ###

Micromouse is an event where small robot mice solve a 16x16 maze.The mice are completely autonomous 
robots that must find their way from a predetermined starting position to the central area of the 
maze unaided. The mouse will need to keep track of where it is, discover walls as it explores, map 
out the maze and detect when it has reached the goal. Having reached the goal, the mouse will 
typically perform additional searches of the maze until it has found an optimal route from the start
to the center. Once the optimal route has been found, the mouse will run that route in the shortest 
possible time.

### Navigation ###

* Attempt 1: 3 ultrasinic sensors (link to wiki, code, build)
* Attempt 2: 1 ultrasonic sensor and 2 infrared sensors (link to wiki, code, build)
* Attempt 3: 3 infrared sensors (link to wiki, code, build)
* Attempt 4: 5 infrared sensors (link to wiki, code, build)

### Cognition ###

* Encoder Breakdown (link to wiki, code, build)
* Encoder Implementation to Navigation (link to wiki, code, build)

* Floodfill Breakdown (link to wiki and example)
* Floodfill Implementation to Navigation

* Implementing Encoders with Floodfill

### Build History ###
(pinouts/diagrams/pics)

* build 1 (board used, chasset used, wheels used... parts list assembled)
* build 2 (3 ultrasinic sensors)
* build 3 (1 ultrasonic sensor and 2 infrared sensors)
* build 4 (3 infrared sensors)
* build h-bridge
* build lipo battery


### Contribution guidelines? ###

* Who did what..
* David: [Stared at wall](https://metrouk2.files.wordpress.com/2014/03/wpid-article-1326118434644-0f63dcba00000578-344670_466x310.jpg) [Thought about doing work](https://www.wikihow.com/images/thumb/e/ea/Stop-Fiddling-with-Your-Braces-Step-2-Version-2.jpg/aid103281-v4-728px-Stop-Fiddling-with-Your-Braces-Step-2-Version-2.jpg)

### External Links ###
* [Presentation](https://drive.google.com/open?id=1HFpVue2xhMitaQs4r1eZ28xImyR0IcPU)
