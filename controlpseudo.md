const int middleIRPin = A1; 
const int leftIRPin  = 13;
const int rightIRPin = 12;
const int rightPulseWM = 11;
const int leftPulseWM = 10;
const int bottomRightIR  = 9;
const int bottomLeftIR = 8;
const int rightMotor2 = 5;
const int rightMotor1 = 4;
const int leftMotor2 = 6;
const int leftMotor1 = 7;
//const int encoder_pinleft = 2;
//const int encoder_pinright = 3;
//unsigned int lc = 0; //initialize left counter
//unsigned int rc = 0; //initialize right counter

void setup()
{
  pinMode(middleIRPin,   INPUT);
  pinMode(leftIRPin,     INPUT);
  pinMode(rightIRPin,    INPUT);
  pinMode(bottomLeftIR,  INPUT);
  pinMode(bottomRightIR, INPUT);
  pinMode(leftMotor1,    OUTPUT);
  pinMode(leftMotor2,    OUTPUT);
  pinMode(rightMotor1,   OUTPUT);
  pinMode(rightMotor2,   OUTPUT);
  pinMode(leftPulseWM,   OUTPUT);
  pinMode(rightPulseWM,  OUTPUT);
  //pinMode(encoder_pinleft, INPUT);
  //pinMode(encoder_pinright, INPUT);
  Serial.begin(9600);

  //attachInterrupt(digitalPinToInterrupt(2),left_count,RISING); //when encoder senses movement
  //attachInterrupt(digitalPinToInterrupt(3),right_count,RISING); //when encoder senses movement
}

void loop()
{   
  long middleDistance, leftDistance, rightDistance, bottomLeftDis, bottomRightDis;



  middleDistance = digitalRead(middleIRPin);
  rightDistance  = digitalRead(rightIRPin);
  leftDistance   = digitalRead(leftIRPin);
  bottomLeftDis  = digitalRead(bottomLeftIR);
  bottomRightDis = digitalRead(bottomRightIR);




  /*Serial.print("Right Distance: ");
  Serial.print(rightDistance);
  Serial.print("\n");
  Serial.print("Left Distance: ");
  Serial.print(leftDistance);
  Serial.print("\n");
  Serial.print("Bottom Right Distance: ");
  Serial.print(bottomRightDis);
  Serial.print("\n");
  Serial.print("Bottom Left Distance: ");
  Serial.print(bottomLeftDis);\
  Serial.print("\n");
  Serial.print("Middle Distance: ");
  Serial.print(middleDistance);
  Serial.print("\n");    
  Serial.print("------------------------------------------------------------\n\n");
  delay(1000);*/
  

/*
  Serial.print("Right Distance: ");
  Serial.print(rc);
  Serial.print("\n");
  Serial.print("Left Distance: ");
  Serial.print(lc);   
  Serial.print("------------------------------------------------------------\n\n");

} */
 



    if(middleDistance == 1)
    {

    if(rightDistance == 0 && leftDistance == 0) goStraight();

    if(rightDistance == 1) correctRight();

    if(leftDistance == 1) correctLeft();

    }
    
    if(middleDistance == 0)
    {
    analogWrite(leftPulseWM,  0);  
    analogWrite(rightPulseWM, 0);
    
    digitalWrite(leftMotor1,  LOW);   
    digitalWrite(leftMotor2,  LOW); 
    digitalWrite(rightMotor1, LOW);  
    digitalWrite(rightMotor2, LOW);

    delay(500);
    
      if(bottomRightDis == 0 && bottomLeftDis == 0) youTurn();

      if(bottomRightDis == 1) rightTurn();

      if(bottomLeftDis == 1) leftTurn();
          
    analogWrite(leftPulseWM,  55);  
    analogWrite(rightPulseWM, 50);
    
    digitalWrite(leftMotor1,  HIGH);   
    digitalWrite(leftMotor2,  LOW); 
    digitalWrite(rightMotor1, HIGH);  
    digitalWrite(rightMotor2, LOW);

    delay(100);
  }
}

 

 
void goStraight()
{
  /*lc = 0;
  left_count();
  while(lc<67){*/
  analogWrite(leftPulseWM, 55);
  analogWrite(rightPulseWM, 50);
  
  digitalWrite(leftMotor1,  HIGH);   
  digitalWrite(leftMotor2,  LOW); 
  digitalWrite(rightMotor1, HIGH);  
  digitalWrite(rightMotor2, LOW);

  //}
}

//void left_count() { lc++; }

//void right_count() { rc++; }



    void correctRight(){
    analogWrite(leftPulseWM,  70);  
    analogWrite(rightPulseWM, 55);
  
    digitalWrite(leftMotor1,  HIGH);   
    digitalWrite(leftMotor2,  LOW); 
    digitalWrite(rightMotor1, HIGH);  
    digitalWrite(rightMotor2, LOW);
    }

    void correctLeft()
    {
    analogWrite(leftPulseWM,  60);  
    analogWrite(rightPulseWM, 70);
    
    digitalWrite(leftMotor1,  HIGH);   
    digitalWrite(leftMotor2,  LOW); 
    digitalWrite(rightMotor1, HIGH);  
    digitalWrite(rightMotor2, LOW);
    }

    
    void rightTurn()
    {
    analogWrite(leftPulseWM,  90);  
    analogWrite(rightPulseWM, 90);
    
    digitalWrite(leftMotor1,  HIGH);   
    digitalWrite(leftMotor2,  LOW); 
    digitalWrite(rightMotor1, LOW);  
    digitalWrite(rightMotor2, HIGH);
    delay(181); 
    }
    
    void leftTurn()
    {
    analogWrite(leftPulseWM,  95);  
    analogWrite(rightPulseWM, 80);
    
    digitalWrite(leftMotor1,  LOW);   
    digitalWrite(leftMotor2,  HIGH); 
    digitalWrite(rightMotor1, HIGH);  
    digitalWrite(rightMotor2, LOW);
    delay(181);
    }


    void youTurn()
    {  
    analogWrite(leftPulseWM,  95);  
    analogWrite(rightPulseWM, 80);
    
    digitalWrite(leftMotor1,  LOW);   
    digitalWrite(leftMotor2,  HIGH); 
    digitalWrite(rightMotor1, HIGH);  
    digitalWrite(rightMotor2, LOW);
    delay(181);
    
    /*digitalWrite(leftMotor1,  LOW);   
    digitalWrite(leftMotor2,  LOW); 
    digitalWrite(rightMotor1, LOW);  
    digitalWrite(rightMotor2, LOW);
    delay(500);*/
    
    analogWrite(leftPulseWM,  95);  
    analogWrite(rightPulseWM, 80);
    
    digitalWrite(leftMotor1,  LOW);   
    digitalWrite(leftMotor2,  HIGH); 
    digitalWrite(rightMotor1, HIGH);  
    digitalWrite(rightMotor2, LOW);
    delay(181);
    }

    
 
